# ActiveInteraction::Sentry

Библиотека для интеграции [ActiveInteraction](https://github.com/AaronLasseigne/active_interaction) с [Sentry](https://github.com/getsentry/sentry-ruby).

Позволяет логировать значения inputs из интеракторов в Sentry.

## Установка

Добавьте строку в `Gemfile`:

```ruby
gem "active_interaction-sentry", git: "https://gitlab.com/evosoft-dev/gems/active_interaction-sentry.git", tag: "v2.0.1"
```

Затем выполните команду:

    $ bundle install

## Использование

[Документация API](https://evosoft-dev.gitlab.io/gems/active_interaction-sentry/ActiveInteraction/Sentry.html)

### Глобальная конфигурация

```ruby
# config/initializers/active_interaction-sentry.rb
ActiveInteraction::Sentry.setup do |config|
  # Подключить библиотеку глобально во все интеракторы
  config.include_globally!

  # Ограничить размер контекста на один интерактор в байтах
  config.max_context_bytesize = 1024

  ##
  # Отфильтровать классы inputs интерактора в режиме "whitelist", которые попадут в контекст
  #
  # По умолчанию в контекст добавляются все inputs
  #
  config.filter_context(
    "TestClass2",
    BigDecimal,
    String,
    Integer,
    mode: :whitelist
  )

  ##
  # Отфильтровать классы inputs интерактора в режиме "blacklist", которые не попадут в контекст
  #
  # Глобальный фильтр может работать только в одном из режимов. Каждый вызов #filter_context перезаписывает предыдущий.
  #
  config.filter_context(
    "TestClass2",
    BigDecimal,
    String,
    Integer,
    mode: :blacklist
  )

  # Преобразовать значения inputs в контексте
  config.map_context(
    # Можно использовать имя метода класса
    "TestClass" => :test,
    # Либо использовать proc
    "TestClass2" => ->(test_class) { test_class.test },
    # Также можно использовать предикаты для выбора классов (аналогично для остальных методов)
    { is_a?: ApplicationRecord } => ->(record) { "#{name}:#{id}" }
  )

  ##
  # Указать, какие классы inputs интерактора будут добавлены в Sentry в качестве тэгов
  #
  # По умолчанию никакие тэги не добавляются
  #
  config.filter_tags(
    test_class: "TestClass",
    test_class2: "TestClass2"
  )

  # Преобразовать значения inputs в тэгах (работает аналогично методу #map_context)
  config.map_tags(
    { is_a?: "TestClass" } => :test,
    "TestClass2" => :test2
  )
end
```

### Локальная конфигурация
  
```ruby
class CropImageInteraction < ActiveInteraction::Base
  # Подключить библиотеку локально в конкретный интерактор (если не подключена глобально)
  include ActiveInteraction::Sentry

  integer :width
  integer :height

  hash :preview do
    integer :width
    integer :height
    integer :compression
  end

  # Добавить значение input в контекст, даже если он глобально запрещён
  sentry_allow_context :width

  # Запретить добавлять значение input в контекст, даже если он глобально разрешён
  #
  # Для вложенных в хэш значений обязателен полный путь
  #
  sentry_deny_context :height, "preview.width"

  # Преобразовать значение input в контексте, даже если глобально есть другое преобразование
  sentry_map_context "preview.height" => ->(height) { height * 2 }
  
  # Добавить тэг
  sentry_allow_tags preview_resolution: :preview

  # Преобразовать значение тэга, даже если глобально есть другое преобразование
  sentry_map_tags preview_resolution: ->(preview) { "#{preview[:width]}x#{preview[:height]}" }

  def execute
    context = Sentry.get_current_scope.contexts.slice(self.class.name)
    tags = Sentry.get_current_scope.tags

    puts "Context:\n#{JSON.pretty_generate(context)}\n"
    puts "Tags:\n#{JSON.pretty_generate(tags)}"
  end
end

> Sentry.init unless Sentry.initialized?
> CropImageInteraction.run!(width: 1920, height: 1080, preview: { width: 640, height: 480, compression: 1 })
Context:
{
  "CropImageInteraction": {
    "width": 1920,
    "preview": {
      "height": 960,
      "compression": 1
    }
  }
}
Tags:
{
  "preview_resolution": "640x480"
}
```

## Разработка

После клонирования репозитория выполните `bin/setup` чтобы установить зависимости. Затем `rake spec` для запуска тестов. Также есть возможность вызвать `bin/console` чтобы запустить консольный интерфейс для экспериментирования.

Для тестирования гема в целевом проекте, его можно установить локально с помощью `rake install`. После чего подключить гем в проект, просто добавив в Gemfile:

```ruby
gem "active_interaction-sentry"
```

## Релиз новой версии

В репозитории используется [семантическое версионирование](http://semver.org/). В частности, для этого используется библиотека [semantic-release](https://www.npmjs.com/package/semantic-release).

Что автоматически выполняется при мерже/коммите в `main`:
- обновление версии в `lib/active_interaction/sentry/version.rb`
- обновление версии в `Gemfile.lock`
- обновление версии в `README.md`
- заполнение `CHANGELOG.md`
- создание тега новой версии в репозитории

Единственное, что требуется для релиза - [корректно описывать коммиты](https://semantic-release.gitbook.io/semantic-release/#how-does-it-work)
