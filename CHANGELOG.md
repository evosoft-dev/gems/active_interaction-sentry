## [2.0.1](https://gitlab.com/evosoft-dev/gems/active_interaction-sentry/compare/v2.0.0...v2.0.1) (2022-10-25)


### Bug Fixes

* bump version ([73ee4fa](https://gitlab.com/evosoft-dev/gems/active_interaction-sentry/commit/73ee4fae9a056651c1bbffddb5694ce14fc0d1a8))

# [2.0.0](https://gitlab.com/evosoft-dev/gems/active_interaction-sentry/compare/v1.0.0...v2.0.0) (2022-10-25)


### Bug Fixes

* :bug: remove deep_dup usage which conflicts with db records ([de19619](https://gitlab.com/evosoft-dev/gems/active_interaction-sentry/commit/de19619ed1fe132fbf976864fb3dbbac8e112df5))


### Features

* add multiple features ([9bc7e50](https://gitlab.com/evosoft-dev/gems/active_interaction-sentry/commit/9bc7e5043688061f629592042ba6cf194ce89d73))


### BREAKING CHANGES

* changed API

# 1.0.0 (2022-10-06)


### Bug Fixes

* fix npm packages and tests ([87d39cf](https://gitlab.com/evosoft-dev/gems/active_interaction-sentry/commit/87d39cfab55444cd2094b76114b5eef232f823f9))
* fix repo url ([0cdc990](https://gitlab.com/evosoft-dev/gems/active_interaction-sentry/commit/0cdc990385df64f0673ba326945655288cff7eef))


### Features

* initial commit ([35349c4](https://gitlab.com/evosoft-dev/gems/active_interaction-sentry/commit/35349c4612db49d0bc0c7786112379cc4d90f961))
