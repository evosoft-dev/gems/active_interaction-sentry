# frozen_string_literal: true

RSpec::Matchers.define :contain_value do |value, path: []|
  match do |actual|
    last_hash = path.size > 1 ? actual.dig(*path[0..-2]) : actual

    return false unless last_hash
    return last_hash.values.include?(value) unless path.last

    last_hash.key?(path.last) && last_hash[path.last] == value
  end

  description do
    path_stringified = path.map { |key| ":#{key}" }.join(", ")

    "contain the \"#{value}\" value on the path \"#{path_stringified}\""
  end
end
