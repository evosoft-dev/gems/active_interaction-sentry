# frozen_string_literal: true

class TestInteraction < ActiveInteraction::Base
  class << self
    def filters=(proc)
      instance_exec(&proc)
    end

    def sentry_allow_context=(filter_names = [])
      sentry_allow_context(*filter_names) if filter_names.present?
    end

    def sentry_deny_context=(filter_names = [])
      sentry_deny_context(*filter_names) if filter_names.present?
    end

    def sentry_map_context=(mapping = {})
      sentry_map_context(**mapping) if mapping.present?
    end

    def sentry_allow_tags=(names_mapping = {})
      sentry_allow_tags(**names_mapping) if names_mapping.present?
    end

    def sentry_deny_tags=(filter_names = [])
      sentry_deny_tags(*filter_names) if filter_names.present?
    end

    def sentry_map_tags=(mapping = {})
      sentry_map_tags(**mapping) if mapping.present?
    end
  end

  def execute
    true
  end
end
