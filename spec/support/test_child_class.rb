# frozen_string_literal: true

require_relative "test_parent_class"

class TestChildClass < TestParentClass; end
