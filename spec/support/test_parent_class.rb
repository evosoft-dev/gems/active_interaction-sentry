# frozen_string_literal: true

class TestParentClass
  attr_reader :id

  def initialize
    @id = SecureRandom.hex
  end

  def ==(other)
    other.instance_of?(self.class) && other.id == id
  end
end
