# frozen_string_literal: true

require "support/test_interaction"

FactoryBot.define do
  factory :active_interaction, class: Class do
    initialize_with do
      # Create anonymous class for safe tests
      Class.new(TestInteraction) do
        # Include functionality on this level is obligatory for safe tests
        include ActiveInteraction::Sentry

        # Fake class name to behave like the original class
        def self.name
          "TestInteraction"
        end
      end
    end

    filters { proc {} }

    sentry_allow_context { [] }
    sentry_deny_context { [] }
    sentry_map_context { {} }

    sentry_allow_tags { {} }
    sentry_deny_tags { [] }
    sentry_map_tags { {} }
  end
end
