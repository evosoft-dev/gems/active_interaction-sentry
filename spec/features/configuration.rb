# frozen_string_literal: true

require "support/test_child_class"

RSpec.shared_context "configuration" do
  let(:sentry_context) { Sentry.get_current_scope.contexts }
  let(:sentry_tags) { Sentry.get_current_scope.tags }

  let(:global_filter_context) { [] }
  let(:global_filter_mode) { :blacklist }
  let(:global_map_context) { {} }

  let(:global_filter_tags) { {} }
  let(:global_map_tags) { {} }

  let(:local_allow_context) { [] }
  let(:local_deny_context) { [] }
  let(:local_map_context) { {} }

  let(:local_allow_tags) { {} }
  let(:local_deny_tags) { [] }
  let(:local_map_tags) { {} }

  let(:active_interaction_sentry_configurator) do
    proc do |config|
      config.filter_context(*global_filter_context, mode: global_filter_mode)
      config.map_context(**global_map_context)
      config.filter_tags(**global_filter_tags)
      config.map_tags(**global_map_tags)
    end
  end

  let(:interaction_filters) { {} }
  let(:interaction_values) { {} }

  let(:interaction_class) do
    build :active_interaction,
      filters:              interaction_filters,
      sentry_allow_context: local_allow_context,
      sentry_deny_context:  local_deny_context,
      sentry_map_context:   local_map_context,
      sentry_allow_tags:    local_allow_tags,
      sentry_deny_tags:     local_deny_tags,
      sentry_map_tags:      local_map_tags
  end

  before do
    Sentry.init

    ActiveInteraction::Sentry.setup do |config|
      active_interaction_sentry_configurator.call(config)
    end

    begin
      interaction_class.new(interaction_values).send(:run!)
    rescue => e
      puts e.message
      puts e.backtrace.join("\n")
      raise
    end
  end

  let(:interaction_filters) do
    proc do
      string :string_param
      hash :hash_param do
        integer :integer_param
        array :array_param do
          string
        end
        date :date_param
      end
      boolean :boolean_param
      hash :hash_param2 do
        date_time :date_time_param
        file :file_param
        record :record_param, class: String
      end
      interface :interface_param, from: TestParentClass
      object :object_param, class: TestParentClass
      decimal :decimal_param
      float :float_param
      symbol :symbol_param
      time :time_param
    end
  end

  let(:interaction_values) do
    {
      string_param: "string_param_value",
      hash_param: {
        integer_param: 10,
        array_param: ["array_param_value1", "array_param_value2"],
        date_param: Date.new(2000, 1, 1)
      },
      boolean_param: true,
      hash_param2: {
        date_time_param: DateTime.new(2000, 1, 1),
        file_param: File.new(__FILE__),
        record_param: "record_param_value"
      },
      interface_param: TestChildClass.new,
      object_param: TestParentClass.new,
      decimal_param: BigDecimal("10.10"),
      float_param: 10.10,
      symbol_param: :test,
      time_param: Time.new(2000, 1, 1)
    }
  end
end
