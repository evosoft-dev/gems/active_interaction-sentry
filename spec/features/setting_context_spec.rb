# frozen_string_literal: true

require_relative "configuration"

RSpec.describe "Setting context", type: :feature do
  include_context "configuration"

  subject { sentry_context }

  context "when filtering" do
    def contain_some_value
      path = [interaction_class.name, :boolean_param]
      value = interaction_values.dig(*path[1..])

      contain_value(value, path: path)
    end

    context "and filters are not configured" do
      let(:active_interaction_sentry_configurator) { ->(config) {} }

      it { is_expected.to have_key(interaction_class.name) }

      it { is_expected.to contain_some_value }
    end

    context "globally" do
      def contain_filtered_value
        path = [interaction_class.name, :hash_param, :integer_param]
        value = interaction_values.dig(*path[1..])

        contain_value(value, path: path)
      end

      context "in whitelist mode" do
        let(:global_filter_mode) { :whitelist }

        context "and no filters are set" do
          let(:global_filter_context) { [] }

          it { is_expected.not_to have_key(interaction_class.name) }
        end

        context "and when the predicate filter is set" do
          let(:global_filter_context) { [{ is_a?: TestParentClass }] }

          it { is_expected.not_to contain_some_value }

          it do
            is_expected.to contain_value interaction_values[:interface_param],
              path: [interaction_class.name, :interface_param]
          end

          it do
            is_expected.to contain_value interaction_values[:object_param],
              path: [interaction_class.name, :object_param]
          end
        end

        context "and the class filter is set" do
          let(:global_filter_context) { [Integer] }

          it { is_expected.not_to contain_some_value }

          it { is_expected.to contain_filtered_value }

          context "and another filter is allowed locally" do
            let(:local_allow_context) { :boolean_param }

            it { is_expected.to contain_some_value }
          end

          context "but denied locally by absolute path" do
            let(:local_deny_context) { "hash_param.integer_param" }

            it { is_expected.not_to contain_filtered_value }
          end

          context "but denied locally by param name" do
            let(:local_deny_context) { :integer_param }

            # Expected not to work, because it's nested param
            it { is_expected.to contain_filtered_value }
          end
        end
      end

      context "in blacklist mode" do
        let(:global_filter_mode) { :blacklist }

        context "and no filters are set" do
          let(:global_filter_context) { [] }

          it { is_expected.to contain_some_value }

          it { is_expected.to contain_filtered_value }
        end

        context "and when the predicate filter is set" do
          let(:global_filter_context) { [{ is_a?: TestParentClass }] }

          it { is_expected.to contain_some_value }

          it do
            is_expected.not_to contain_value interaction_values[:interface_param],
              path: [interaction_class.name, :interface_param]
          end

          it do
            is_expected.not_to contain_value interaction_values[:object_param],
              path: [interaction_class.name, :object_param]
          end
        end

        context "and the class filter is set" do
          let(:global_filter_context) { [Integer] }

          it { is_expected.not_to contain_filtered_value }

          context "and another filter is denied locally" do
            let(:local_deny_context) { :boolean_param }

            it { is_expected.not_to contain_some_value }
          end

          context "but allowed locally by param name" do
            let(:local_allow_context) { :integer_param }

            # Expected not to work, because it's nested param
            it { is_expected.not_to contain_filtered_value }
          end

          context "but allowed locally by absolute path" do
            let(:local_allow_context) { "hash_param.integer_param" }

            it { is_expected.to contain_filtered_value }
          end
        end
      end
    end
  end

  context "when mapping" do
    def contain_mapped_value(method = :upcase, value: nil)
      path = [interaction_class.name, :string_param]
      value ||= interaction_values.dig(*path[1..]).public_send(method)

      contain_value(value, path: path)
    end

    context "and mapping is not set" do
      it { is_expected.not_to contain_mapped_value }
    end

    describe "globally" do
      let(:global_map_context) { { String => :upcase } }

      context "with a predicate" do
        let(:global_map_context) { { { is_a?: TestParentClass } => :id } }

        it do
          is_expected.to contain_value interaction_values[:interface_param].id,
            path: [interaction_class.name, :interface_param]
        end

        it do
          is_expected.to contain_value interaction_values[:object_param].id,
            path: [interaction_class.name, :object_param]
        end
      end

      context "with a symbolic method name" do
        it { is_expected.to contain_mapped_value }
      end

      context "with a proc" do
        let(:global_map_context) { { String => proc { |s| s.upcase } } }

        it { is_expected.to contain_mapped_value }
      end

      context "with a proc without arguments" do
        let(:global_map_context) { { String => proc { :test_value } } }

        it { is_expected.to contain_mapped_value(value: :test_value) }
      end

      context "but overrided locally" do
        let(:local_map_context) { { string_param: :capitalize } }

        it { is_expected.to contain_mapped_value(:capitalize) }
      end
    end

    describe "locally" do
      let(:local_map_context) { { string_param: :capitalize } }

      it { is_expected.to contain_mapped_value(:capitalize) }
    end
  end

  context "when setting max context bytesize" do
    let(:bytesize) { 500 }

    let(:active_interaction_sentry_configurator) do
      ->(config) {
        config.max_context_bytesize = bytesize
      }
    end

    subject { super()[interaction_class.name].to_json.bytesize }

    it { is_expected.to eq bytesize }
  end

  context "when unexpected error occurs" do
    subject { interaction_class.new(interaction_values).send(:run!) }

    let(:error) { StandardError.new }

    before { allow(ActiveInteraction::Sentry::Core).to receive(:collect_context).and_raise(error) }

    specify { expect { subject }.not_to raise_error }

    it "sends an error to Sentry" do
      expect(Sentry).to receive(:capture_exception).with(error)

      subject
    end
  end
end
