# frozen_string_literal: true

require_relative "configuration"

RSpec.describe "Setting tags", type: :feature do
  include_context "configuration"

  subject { sentry_tags }

  let(:tag_name) { :example_param }

  context "when filtering" do
    def include_filtered_tag
      eq(tag_name => interaction_values.fetch(:symbol_param))
    end

    context "and no filters are set" do
      it { is_expected.to be_empty }
    end

    context "globally" do
      let(:global_filter_tags) { { tag_name => Symbol } }

      it { is_expected.to include_filtered_tag }

      context "with a predicate filter" do
        let(:global_filter_tags) { { tag_name => { instance_of?: TestChildClass } } }

        it { is_expected.to eq(tag_name => interaction_values.fetch(:interface_param)) }
      end

      context "but denied locally" do
        let(:local_deny_tags) { :symbol_param }

        it { is_expected.not_to include_filtered_tag }
      end
    end

    context "locally" do
      let(:local_allow_tags) { { tag_name => :symbol_param } }

      it { is_expected.to include_filtered_tag }
    end
  end

  context "when mapping" do
    def include_mapped_tag(method = :upcase, value: nil)
      eq(tag_name => value || interaction_values.fetch(:symbol_param).public_send(method))
    end

    context "globally" do
      let(:global_filter_tags) { { tag_name => Symbol } }
      let(:global_map_tags) { { Symbol => :upcase } }

      context "with a predicate" do
        let(:global_filter_tags) { { test_child: TestChildClass, test_parent: TestParentClass } }
        let(:global_map_tags) { { { is_a?: TestParentClass } => :id } }

        it do
          is_expected.to eq \
            test_child: interaction_values.fetch(:interface_param).id,
            test_parent: interaction_values.fetch(:object_param).id
        end
      end

      context "with a symbolic method name" do
        it { is_expected.to include_mapped_tag }
      end

      context "with a proc" do
        let(:global_map_tags) { { Symbol => proc { |s| s.upcase } } }

        it { is_expected.to include_mapped_tag }
      end

      context "with a proc without arguments" do
        let(:global_map_tags) { { Symbol => proc { :test_value } } }

        it { is_expected.to include_mapped_tag(value: :test_value) }
      end

      context "but overrided locally" do
        let(:local_map_tags) { { tag_name => :capitalize } }

        it { is_expected.to include_mapped_tag(:capitalize) }
      end
    end

    context "locally" do
      let(:local_allow_tags) { { tag_name => :symbol_param } }
      let(:local_map_tags) { { tag_name => :capitalize } }

      it { is_expected.to include_mapped_tag(:capitalize) }
    end
  end

  context "when unexpected error occurs" do
    subject { interaction_class.new(interaction_values).send(:run!) }

    let(:error) { StandardError.new }

    before { allow(ActiveInteraction::Sentry::Core).to receive(:collect_tags).and_raise(error) }

    specify { expect { subject }.not_to raise_error }

    it "sends an error to Sentry" do
      expect(Sentry).to receive(:capture_exception).with(error)

      subject
    end
  end
end
