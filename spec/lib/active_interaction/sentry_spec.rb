# frozen_string_literal: true

RSpec.describe ActiveInteraction::Sentry, type: :lib do
  describe "library configuration" do
    context "when the library was included globally" do
      subject { ActiveInteraction::Base }

      before do
        # Stub class avoiding real inclusion to ensure tests isolation
        stub_const("ActiveInteraction::Base", ActiveInteraction::Base.dup)

        ActiveInteraction::Sentry.setup(&:include_globally!)
      end

      it { is_expected.to include ActiveInteraction::Sentry }
    end

    context "when the library wasn't included globally" do
      subject { ActiveInteraction::Base }

      it { is_expected.not_to include ActiveInteraction::Sentry }
    end
  end
end
