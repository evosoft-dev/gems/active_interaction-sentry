# frozen_string_literal: true

require_relative "core/context_spec"
require_relative "core/selectors_spec"
require_relative "core/tags_spec"

RSpec.describe ActiveInteraction::Sentry::Core, type: :lib do
  it_behaves_like "context"
  it_behaves_like "selectors"
  it_behaves_like "tags"
end
