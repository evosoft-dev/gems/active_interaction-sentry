# frozen_string_literal: true

RSpec.shared_examples "selectors" do
  describe ".class_selector" do
    context "when class is unknown" do
      let(:klass) { :unknown }

      it "raises correct error" do
        expect { subject.send(:class_selector, 1).call(klass) }
          .to raise_error(ArgumentError, "Invalid class selector: #{klass.inspect}")
      end
    end
  end
end
