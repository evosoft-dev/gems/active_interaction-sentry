# frozen_string_literal: true

RSpec.shared_examples "tags" do
  describe ".map_tags" do
    context "when mapping is unknown" do
      let(:mapping) { 123 }

      let(:tags) { { key: "value" } }
      let(:local_tags_mapping) { { tags.keys.first => mapping } }

      it "raises correct error" do
        expect { subject.send(:map_tags, tags, local_tags_mapping) }
          .to raise_error(ArgumentError, "Invalid mapping type: #{mapping.class}")
      end
    end
  end
end
