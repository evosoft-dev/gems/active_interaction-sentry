# frozen_string_literal: true

RSpec.shared_examples "context" do
  describe ".map_context" do
    context "when mapping is unknown" do
      let(:mapping) { 123 }

      let(:interaction_filters) { { key: ActiveInteraction::Filter.new(:test) } }
      let(:interaction_inputs) { { interaction_filters.keys.first => :test_value } }
      let(:local_context_mapping) { { interaction_filters.keys.first => mapping } }

      it "raises correct error" do
        expect { subject.send(:map_context, interaction_filters, interaction_inputs, local_context_mapping) }
          .to raise_error(ArgumentError, "Invalid mapping type: #{mapping.class}")
      end
    end
  end
end
