# frozen_string_literal: true

require "active_interaction"
require "sentry-ruby"

require "dry-types"
require "active_support/core_ext/module/concerning"
require "active_support/core_ext/hash/indifferent_access"
require "active_support/core_ext/array/wrap"

require "zeitwerk"

loader = Zeitwerk::Loader.new
loader.tag = File.basename(__FILE__, ".rb")
loader.inflector = Zeitwerk::GemInflector.new(__FILE__)
loader.push_dir("#{__dir__}/..")
loader.setup

##
# ActiveInteraction Sentry integration
#
module ActiveInteraction::Sentry
  # Extension methods to ActiveInteraction::Base
  module Extension; end

  ##
  # Define variables, methods and callbacks on interaction class
  #
  # @param base [ActiveInteraction::Base] interaction class
  #
  def self.included(base)
    base.include Extension::InstanceMethods
    base.extend Extension::ClassMethods

    base.class_eval do
      set_callback :execute, :before, :set_sentry_context
      set_callback :execute, :before, :set_sentry_tags

      class_attribute :sentry_context_filters
      class_attribute :sentry_tags_filters
      class_attribute :sentry_context_mapping
      class_attribute :sentry_tags_mapping

      base.sentry_context_filters = { whitelist: [], blacklist: [] }.freeze
      base.sentry_tags_filters = { whitelist: {}, blacklist: [] }.freeze
      base.sentry_context_mapping = {}
      base.sentry_tags_mapping = {}
    end
  end

  ##
  # (see ActiveInteraction::Sentry::Core::Configuration#setup)
  #
  def self.setup(&block)
    ActiveInteraction::Sentry::Core.setup(&block)
  end
end

loader.eager_load
