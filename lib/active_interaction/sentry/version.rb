# frozen_string_literal: true

##
# ActiveInteraction module
#
module ActiveInteraction
  module Sentry
    # Current version of the gem
    VERSION = "2.0.1"
  end
end
