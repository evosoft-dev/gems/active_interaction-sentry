# frozen_string_literal: true

##
# Instance methods to include to an interaction
#
module ActiveInteraction::Sentry::Extension::InstanceMethods
  ##
  # Callback method to collect data and set Sentry context
  #
  def set_sentry_context
    context = ActiveInteraction::Sentry::Core.collect_context \
      interaction_filters: self.class.filters,
      interaction_inputs: inputs,
      local_context_filters: self.class.sentry_context_filters,
      local_context_mapping: self.class.sentry_context_mapping

    Sentry.set_context(self.class.name, context) if context.present?
  rescue => e
    Sentry.capture_exception(e)
  end

  ##
  # Callback method to collect data and set Sentry tags
  #
  def set_sentry_tags
    tags = ActiveInteraction::Sentry::Core.collect_tags \
      interaction_filters: self.class.filters,
      interaction_inputs: inputs,
      local_tags_filters: self.class.sentry_tags_filters,
      local_tags_mapping: self.class.sentry_tags_mapping

    Sentry.set_tags(tags)
  rescue => e
    Sentry.capture_exception(e)
  end
end
