# frozen_string_literal: true

##
# Class methods to include to an interaction
#
module ActiveInteraction::Sentry::Extension::ClassMethods
  include ActiveInteraction::Sentry::Core::Validation

  ##
  # Allows specified interaction input names to be added to the Sentry context
  #
  # Overrides global configuration
  #
  # @param input_names [Array<Symbol | String>] symbol input name or string path like "a.b.c.d"
  #
  def sentry_allow_context(*input_names)
    sentry_set_filters :sentry_context_filters, :whitelist, input_names,
      input_names_validator: Types::LocalContextFilter
  end

  ##
  # Forbids specified interaction input names to be added in the Sentry context
  #
  # Overrides global configuration
  #
  # @param input_names [Array<Symbol | String>] symbol input name or string path like "a.b.c.d"
  #
  def sentry_deny_context(*input_names)
    sentry_set_filters :sentry_context_filters, :blacklist, input_names,
      input_names_validator: Types::LocalContextFilter
  end

  ##
  # Allows specified interaction input names to be added in the Sentry tags
  #
  # Overrides global configuration
  #
  # @param input_names [Hash<Symbol, Symbol | String>]
  #   - keys are tag names
  #   - values are input name or string path like "a.b.c.d"
  #
  def sentry_allow_tags(**input_names)
    sentry_set_filters :sentry_tags_filters, :whitelist, input_names,
      input_names_validator: Types::LocalTagsWhitelistFilter
  end

  ##
  # Forbids specified interaction input names to be added in the Sentry tags
  #
  # Overrides global configuration
  #
  # @param input_names [Array<Symbol | String>] symbol input name or string path like "a.b.c.d"
  #
  def sentry_deny_tags(*input_names)
    sentry_set_filters :sentry_tags_filters, :blacklist, input_names,
      input_names_validator: Types::LocalTagsBlacklistFilter
  end

  ##
  # Converts specified interaction input names to a new format
  #
  # Overrides global configuration
  #
  # @param mapping [Hash<Symbol | String, Symbol | Proc>]
  #   - keys are input name or string path like "a.b.c.d"
  #   - values are symbol method names or procs
  #
  def sentry_map_context(**mapping)
    sentry_set_mapping :sentry_context_mapping, mapping,
      input_names_validator: Types::LocalContextMapping
  end

  ##
  # Converts specified tags to a new format
  #
  # Overrides global configuration
  #
  # @param mapping [Hash<Symbol, Symbol | Proc>]
  #   - keys are already filtered tag names
  #   - values are symbol method names or procs
  #
  def sentry_map_tags(**mapping)
    sentry_set_mapping :sentry_tags_mapping, mapping,
      input_names_validator: Types::LocalTagsMapping
  end

  private

  ##
  # Validate input names
  #
  # @param input_names [Array<Symbol | String> | Hash<Symbol, Symbol | String>] input names to validate
  # @param validator [Dry::Types::Definition] validator for input names
  # @param target_input_names [Hash<Hash | Array, String>] input names with which there should be no common elements
  #
  def sentry_validate_input_names(input_names, validator, **target_input_names)
    input_names = validator.call(input_names)

    keys = input_names.try { try(:keys) || self }.map(&:to_sym)

    raise ArgumentError, "Input names has duplicates" unless keys.uniq.size == keys.size

    keys.each do |key|
      target_input_names.each do |names, error_message|
        target_keys = names.try { try(:keys) || self }.map(&:to_sym)

        raise ArgumentError, error_message % key if target_keys.include?(key)
      end
    end
  end

  ##
  # Set filters
  #
  # @param var_name [Symbol] class attribute of (see ActiveInteraction::Sentry)
  # @param mode [:whitelist, :blacklist] Filtering mode
  # @param input_names [Array<Symbol | String> | Hash<Symbol, Symbol | String>] input names to set
  # @param input_names_validator [Dry::Types::Definition] validator for input names
  #
  def sentry_set_filters(var_name, mode, input_names, input_names_validator:)
    mode = Types::FilterMode[mode]
    filters = public_send(var_name).fetch(mode)

    opposite_mode = mode == :whitelist ? :blacklist : :whitelist
    opposite_filters = public_send(var_name).fetch(opposite_mode)

    sentry_validate_input_names input_names, input_names_validator,
      filters          => "Input name %s is already declared in #{mode} filter",
      opposite_filters => "Input name %s is already declared in #{opposite_mode} filter"

    if filters.is_a?(::Hash)
      filters.merge!(input_names)
    else
      filters.push(*input_names)
    end
  end

  ##
  # Set mappings
  #
  # @param var_name [Symbol] class attribute of {ActiveInteraction::Sentry}
  # @param input_names [Hash<Symbol | String, Symbol | Proc>] input names to set
  # @param input_names_validator [Dry::Types::Definition] validator for input names
  #
  def sentry_set_mapping(var_name, input_names, input_names_validator:)
    mapping = public_send(var_name)

    sentry_validate_input_names input_names, input_names_validator,
      mapping => "Input name %s is already declared in mapping"

    mapping.merge!(input_names)
  end
end
