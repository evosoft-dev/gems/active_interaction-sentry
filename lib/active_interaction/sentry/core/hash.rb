# frozen_string_literal: true

##
# Hash processing methods
#
module ActiveInteraction::Sentry::Core::Hash
  private

  ##
  # Traverse nested hash and yield path and value
  #
  # @param hash [Hash]
  # @yield [Array, Object]
  #
  def deep_traverse_hash(hash)
    stack = hash.map { |k, v| [[k], v] }

    until stack.empty?
      key, value = stack.shift

      yield(key, value)

      value.each { |k, v| stack.push [key.dup << k, v] } if value.is_a?(Hash)
    end
  end

  ##
  # Truncate hash to max bytesize
  #
  # @param hash [Hash]
  # @param max_bytesize [Integer]
  #
  # @return [Hash]
  #
  def truncate_hash(hash, max_bytesize)
    return hash unless hash.to_json.bytesize > max_bytesize

    new_hash = {}

    path_values = []

    deep_traverse_hash(hash) do |path, value|
      path_values << [path, value] unless value.is_a?(Hash)

      hash_set_by_path(new_hash, path, value)
    end

    path_values.sort_by! { |_, v| -v.to_json.bytesize }.each do |path, value|
      bytesize = new_hash.to_json.bytesize

      break unless bytesize > max_bytesize

      value = value.to_s.truncate_bytes([3, value.to_s.bytesize - (bytesize - max_bytesize)].max)

      hash_set_by_path(new_hash, path, value)
    end

    new_hash
  end

  ##
  # Set hash element by path
  #
  # Creates nested hashes if necessary
  #
  # @param hash [Hash]
  # @param path [Array<Symbol>]
  # @param value [Object]
  #
  # @return [Hash]
  #
  def hash_set_by_path(hash, path, value)
    *head, tail = path
    head.inject(hash) { |acc, elem| acc[elem] ||= {} }[tail] = value

    hash
  end
end
