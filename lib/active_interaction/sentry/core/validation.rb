# frozen_string_literal: true

##
# Validation helpers
#
module ActiveInteraction::Sentry::Core::Validation
  ##
  # Validation types
  #
  module Types
    send :include, Dry.Types(default: :strict) # Bypass Yard warning

    # Method name or Proc
    MethodNameOrProc = Symbol | Instance(Proc)

    # Class or stringified class for later constantize
    LateInitClass = Class | String

    # Class selector
    ClassSelector = LateInitClass | Hash.map(Symbol, LateInitClass).constrained(size: 1)

    # Path selector
    PathSelector = Symbol | String

    # Filter mode
    FilterMode = Symbol.enum(:whitelist, :blacklist)

    # Local context filter type
    LocalContextFilter = Array.of(PathSelector)

    # Local tags filter whitelist type
    LocalTagsWhitelistFilter = Hash.map(Symbol, PathSelector)

    # Local tags filter blacklist type
    LocalTagsBlacklistFilter = Array.of(PathSelector)

    # Local context mapping type
    LocalContextMapping = Strict::Hash.map(PathSelector, MethodNameOrProc)

    # Local tags mapping type
    LocalTagsMapping = Strict::Hash.map(Symbol, MethodNameOrProc)

    # Global context filter type
    GlobalContextFilter = Array.of(ClassSelector)

    # Global tags filter type
    GlobalTagsFilter = Hash.map(Symbol, ClassSelector)

    # Global mapping type
    GlobalMapping = Strict::Hash.map(ClassSelector, MethodNameOrProc)
  end
end
