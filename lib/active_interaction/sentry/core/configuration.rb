# frozen_string_literal: true

##
# Library configuration
#
module ActiveInteraction::Sentry::Core::Configuration
  ##
  # Set builder when extended
  #
  # @param base [ActiveInteraction::Base] interaction class
  #
  def self.extended(base)
    base.class_eval do
      @builder = ActiveInteraction::Sentry::Core::Configuration::Builder.new
    end
  end

  Builder.instance_methods(false)
         .reject { |method| method.to_s.end_with?("=") }
         .each { |method| delegate method, to: :@builder }

  ##
  # Configure library
  #
  # @yield [ActiveInteraction::Sentry::Config::Builder]
  #
  # @example
  #   ActiveInteraction::Sentry.setup do |config|
  #     config.max_context_bytesize = 8000
  #     config.include_globally!
  #   end
  #
  def setup(&block)
    @builder = Builder.new(&block)
  end
end
