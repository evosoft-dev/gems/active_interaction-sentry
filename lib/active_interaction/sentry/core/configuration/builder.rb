# frozen_string_literal: true

##
# Configuration builder
#
class ActiveInteraction::Sentry::Core::Configuration::Builder
  include ActiveInteraction::Sentry::Core::Validation

  ##
  # Maximum allowed Sentry context bytesize
  #
  # @return [Integer]
  #
  attr_accessor :max_context_bytesize

  ##
  # Global context filtering types
  #
  # @return [Array<Class>]
  #
  attr_reader :global_context_filter_types

  ##
  # Global context filter mode
  #
  # @return [:whitelist, :blacklist]
  #
  attr_reader :global_context_filter_mode

  ##
  # Global context mapping of classes to function name or function
  #
  # @return [Hash<Class, Symbol | Proc>]
  #
  attr_reader :global_context_mapping

  ##
  # Global tags filters
  #
  # @return [Hash<Class, Symbol>]
  #
  attr_reader :global_tags_filters

  ##
  # Global tags mapping of classes to function name or function
  #
  # @return [Hash<Class, Symbol | Proc>]
  #
  attr_reader :global_tags_mapping

  def initialize
    # Set default values
    self.max_context_bytesize = 8000

    filter_context mode: :blacklist
    map_context

    filter_tags
    map_tags

    yield(self) if block_given?
  end

  ##
  # Globally include Sentry to ActiveInteraction
  #
  def include_globally!
    ActiveInteraction::Base.class_eval { include ActiveInteraction::Sentry }
  end

  ##
  # Configure context filters
  #
  # @param types [Array<Class | Hash<Symbol, Class>>] Filtered class names
  # @param mode [:whitelist, :blacklist] Filtering mode
  #
  def filter_context(*types, mode:)
    @global_context_filter_mode = Types::FilterMode[mode].freeze

    @global_context_filter_types = Types::GlobalContextFilter[types].freeze
  end

  ##
  # Configure tags filters
  #
  # @param filters [Hash<Symbol, Class | Hash<Symbol, Class>>] Pairs of tag name and class
  #
  def filter_tags(**filters)
    @global_tags_filters = Types::GlobalTagsFilter[filters].freeze
  end

  ##
  # Configure context mapping
  #
  # @param mapping [Hash<Class | Hash<Symbol, Class>, Symbol | Proc>] Mapping of classes to function name or function
  #
  def map_context(**mapping)
    @global_context_mapping = \
      Types::GlobalMapping[mapping]
      .sort_by { |k, _| k.instance_of?(Class) ? 0 : 1 }
      .to_h
      .freeze
  end

  ##
  # Configure tags mapping
  #
  # @param mapping [Hash<Class | Hash<Symbol, Class>, Symbol | Proc>] Mapping of classes to function name or function
  #
  def map_tags(**mapping)
    @global_tags_mapping = \
      Types::GlobalMapping[mapping]
      .sort_by { |k, _| k.instance_of?(Class) ? 0 : 1 }
      .to_h
      .freeze
  end
end
