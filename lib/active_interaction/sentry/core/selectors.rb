# frozen_string_literal: true

##
# Validation helpers
#
module ActiveInteraction::Sentry::Core::Selectors
  private

  ##
  # Class selector
  #
  # @param value [Object] tested value
  # @param index [Integer] class argument index
  #
  # @return [Proc] selector method
  #
  # @example Search in an array of classes
  #   [String, Integer].find(&class_selector("a")) # => String
  #
  #   Which is equivalent to: [String, Integer].find { |klass| "a".instance_of?(klass) }
  #
  # @example Search in an array of class selectors
  #   [{ is_a?: String }, { is_a?: Integer }].find(&class_selector("a")) # => { is_a?: String }
  #
  #   Which es equivalent to: [{ is_a?: String }, { is_a?: Integer }].find { |klass| "a".public_send(*klass.first) }
  #
  # @example Search in an array of pairs where class selector is at index 0
  #   [[String, :to_symbol], [Integer, :to_s]].find(&class_selector("a")) # => [String, :to_symbol]
  #
  #   Which es equivalent to: [[String, :to_symbol], [Integer, :to_s]].find { |klass,| "a".instance_of?(klass) }
  #
  # @example Search in an array of pairs where class selector is at index 1
  #   [[:tag_a, String], [:tag_b, Integer]].find(&class_selector("a", index: 1)) # => [:tag_a, String]
  #
  #   Which es equivalent to: [[:tag_a, String], [:tag_b, Integer]].find { |_, klass| "a".instance_of?(klass) }
  #
  def class_selector(value, index: 0)
    proc do |args|
      klass = Array.wrap(args).fetch(index)

      case klass
      when Class, String
        value.instance_of?(klass.try { try(:constantize) || self })
      when Hash
        method, klass = klass.first
        value.public_send(method, klass.try { try(:constantize) || self })
      else
        raise ArgumentError, "Invalid class selector: #{klass.inspect}"
      end
    end
  end

  ##
  # Path selector
  #
  # @param value [Array<Symbol>] tested path value
  # @param index [Integer] path argument index
  #
  # @return [Proc] selector method
  #
  # @example Search key in a path
  #   [:a, :b, :c].find(&path_selector([:a])) # => :a
  #   or
  #   ["a", "b", "c"].find(&path_selector([:a])) # => :a
  #
  #   Which is equivalent to: [:a, :b, :c].find { |key| key.to_sym == :a }
  #
  # @example Search in an array of path selectors
  #   ["a.b.c", "a.b", "b.c"].find(&path_selector([:a, :b, :c])) # => "a.b.c"
  #
  #   Which is equivalent to: ["a.b.c", "a.b", "b.c"].find { |path| path.split(".").map(&:to_sym) == [:a, :b, :c] }
  #
  # @example Search in an array of pairs where path selector is at index 0
  #   [[:a, :to_symbol], [:b, :to_s]].find(&path_selector([:a, :b, :c])) # => [:a, :to_symbol]
  #
  #   Which es equivalent to: [[:a, :to_symbol], [:b, :to_s]].find { |path,| [:a, :b, :c].first == path.to_sym }
  #
  # @example Search in an array of pairs where path selector is at index 1
  #   [[:tag_a, :a], [:tag_b, :b]].find(&path_selector([:a, :b, :c], index: 1)) # => [:tag_a, :a]
  #
  #   Which es equivalent to: [[:tag_a, :a], [:tag_b, :b]].find { |_, path| [:a, :b, :c].first == path.to_sym }
  #
  def path_selector(value, index: 0)
    proc do |args|
      path = Array.wrap(args).fetch(index).to_s

      if path[/\w\.\w/]
        value == path.split(".").map(&:to_sym)
      else
        value.size == 1 && value.first == path.to_sym
      end
    end
  end
end
