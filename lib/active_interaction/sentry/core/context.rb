# frozen_string_literal: true

##
# Sentry context methods
#
module ActiveInteraction::Sentry::Core::Context
  ##
  # Collect context
  #
  # Filters, maps and truncates interaction inputs
  #
  # @param interaction_filters [Hash<Symbol, ActiveInteraction::Filter>]
  # @param interaction_inputs [Hash]
  # @param local_context_filters [Hash]
  # @param local_context_mapping [Hash]
  #
  # @return [Hash]
  #
  def collect_context(interaction_filters:, interaction_inputs:, local_context_filters:, local_context_mapping:)
    interaction_inputs = filter_context(interaction_filters, interaction_inputs, local_context_filters)

    interaction_inputs = map_context(interaction_filters, interaction_inputs, local_context_mapping)

    truncate_hash(interaction_inputs, max_context_bytesize)
  end

  private

  ##
  # Filter context
  #
  # Removes all keys that are not whitelisted or are blacklisted in the configuration
  #
  # @param interaction_filters [Hash<Symbol, ActiveInteraction::Filter>]
  # @param interaction_inputs [Hash<Symbol, Object>]
  # @param local_context_filters [Hash]
  #
  def filter_context(interaction_filters, interaction_inputs, local_context_filters)
    filtered_inputs = {}

    deep_traverse_inputs(interaction_filters, interaction_inputs) do |path, value|
      unless local_context_filters.fetch(:whitelist).any?(&path_selector(path))
        next if local_context_filters.fetch(:blacklist).any?(&path_selector(path))
        next if global_context_filter_types.any?(&class_selector(value)) == (global_context_filter_mode == :blacklist)
      end

      next if value.is_a?(Hash)

      hash_set_by_path(filtered_inputs, path, value)
    end

    filtered_inputs
  end

  ##
  # Map context
  #
  # @param interaction_filters [Hash<Symbol, ActiveInteraction::Filter>]
  # @param interaction_inputs [Hash<Symbol, Object>]
  # @param local_context_mapping [Hash]
  #
  def map_context(interaction_filters, interaction_inputs, local_context_mapping)
    mapped_inputs = {}

    deep_traverse_inputs(interaction_filters, interaction_inputs) do |path, value|
      next if value.nil?

      mapping = local_context_mapping.find(&path_selector(path))&.last

      mapping ||= global_context_mapping.find(&class_selector(value))&.last

      new_value = case mapping
                  when nil
                    value
                  when Symbol
                    value.method(mapping).call
                  when Proc
                    if mapping.arity == 1
                      mapping.call(value)
                    else
                      mapping.call
                    end
                  else
                    raise ArgumentError, "Invalid mapping type: #{mapping.class}"
                  end

      hash_set_by_path(mapped_inputs, path, new_value)
    end

    mapped_inputs
  end
end
