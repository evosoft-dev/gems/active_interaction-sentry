# frozen_string_literal: true

##
# ActiveInteraction inputs processing methods
#
module ActiveInteraction::Sentry::Core::Inputs
  private

  ##
  # Traverse ActiveInteraction inputs
  #
  # @param filters [Hash<Symbol, ActiveInteraction::Filter>]
  # @param inputs [Hash<Symbol, Object>]
  #
  # @yield [Array, Object, Symbol] name, value, path
  #
  def deep_traverse_inputs(filters, inputs, &block)
    stack = filters.map { |name, filter| [[name], filter] }

    results = []

    until stack.empty?
      path, filter = stack.shift

      value = inputs.dig(*path)

      results << [path, value]

      next if filter.is_a?(ActiveInteraction::ArrayFilter)

      filter.filters.each { |name, f| stack.push [path.dup << name, f] }
    end

    results.each(&block)
  end
end
