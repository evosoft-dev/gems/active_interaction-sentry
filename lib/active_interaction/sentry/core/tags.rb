# frozen_string_literal: true

##
# Sentry tags methods
#
module ActiveInteraction::Sentry::Core::Tags
  ##
  # Collect tags
  #
  # Filters, maps interaction inputs
  #
  # @param interaction_filters [Hash<Symbol, ActiveInteraction::Filter>]
  # @param interaction_inputs [Hash<Symbol, Object>]
  # @param local_tags_filters [Hash]
  # @param local_tags_mapping [Hash]
  #
  # @return [Hash]
  #
  def collect_tags(interaction_filters:, interaction_inputs:, local_tags_filters:, local_tags_mapping:)
    tags = filter_tags(interaction_filters, interaction_inputs, local_tags_filters)

    map_tags(tags, local_tags_mapping)
  end

  private

  ##
  # Filter tags
  #
  # Removes all keys that are not whitelisted or are blacklisted in the configuration
  #
  # @param interaction_filters [Hash<Symbol, ActiveInteraction::Filter>]
  # @param interaction_inputs [Hash<Symbol, Object>]
  # @param local_tags_filters [Hash]
  #
  # @return [Hash<Symbol, Object>] Hash with filtered tags
  #
  def filter_tags(interaction_filters, interaction_inputs, local_tags_filters)
    tags = {}

    deep_traverse_inputs(interaction_filters, interaction_inputs) do |path, value|
      allowed_type = local_tags_filters.fetch(:whitelist).find(&path_selector(path, index: 1))

      next if !allowed_type && local_tags_filters.fetch(:blacklist).any?(&path_selector(path))

      allowed_type ||= global_tags_filters.find(&class_selector(value, index: 1))

      next unless allowed_type

      tags[allowed_type.first] = value
    end

    tags
  end

  ##
  # Map tags
  #
  # @param tags [Hash<Symbol, Object>]
  # @param local_tags_mapping [Hash]
  #
  def map_tags(tags, local_tags_mapping)
    new_tags = {}

    tags.each do |name, value|
      mapping = local_tags_mapping[name]

      mapping ||= global_tags_mapping.find(&class_selector(value))&.last

      new_value = case mapping
                  when nil
                    value
                  when Symbol
                    value.method(mapping).call
                  when Proc
                    if mapping.arity == 1
                      mapping.call(value)
                    else
                      mapping.call
                    end
                  else
                    raise ArgumentError, "Invalid mapping type: #{mapping.class}"
                  end

      new_tags[name] = new_value
    end

    new_tags
  end
end
