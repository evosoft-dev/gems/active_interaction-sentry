# frozen_string_literal: true

##
# Core functionality module
#
module ActiveInteraction::Sentry::Core
  extend ActiveInteraction::Sentry::Core::Configuration
  extend ActiveInteraction::Sentry::Core::Context
  extend ActiveInteraction::Sentry::Core::Hash
  extend ActiveInteraction::Sentry::Core::Inputs
  extend ActiveInteraction::Sentry::Core::Selectors
  extend ActiveInteraction::Sentry::Core::Tags
end
